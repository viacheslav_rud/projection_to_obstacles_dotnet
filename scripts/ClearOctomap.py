#!/usr/bin/env python
from std_srvs.srv import Empty
import rospy

rospy.init_node("clear_octomap")
resetService = rospy.ServiceProxy("/octomap_server/reset", Empty)
resetService.wait_for_service()
resetService.call()

while(True):
    rospy.sleep(60)
    resetService.call()
