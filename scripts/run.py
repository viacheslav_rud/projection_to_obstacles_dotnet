#!/usr/bin/env python

import subprocess
import atexit
import os
import sys
import rospy


def main(args):
    rospy.init_node("projection_to_obstacles_dotnet")
    rospy.loginfo("projection_to_obstacles_dotnet started. Args: " + " ".join(args))
    rospy.sleep(5)
    base = os.environ["ROS_PACKAGE_PATH"].split(':')[0]
    command = "/projection_to_obstacles_dotnet/projection_to_obstacles_dotnet/Build/projection_to_obstacles_dotnet"
    command = base + command
    command = command.strip()
    fullCommand = [command]
    fullCommand.extend(args)
    process = subprocess.Popen(
        fullCommand, stdout=subprocess.PIPE, close_fds=True)
    for c in iter(lambda: process.stdout.read(), ''):
        rospy.loginfo(c)

    atexit.register(process.kill)


if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except rospy.ROSInterruptException as e:
        rospy.logerr(e.msg)
        pass
