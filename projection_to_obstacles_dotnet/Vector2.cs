namespace projection_to_obstacles_dotnet
{
    public readonly struct Vector2
    {
        public readonly float x;
        public readonly float y;
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }
}