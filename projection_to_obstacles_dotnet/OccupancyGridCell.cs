namespace projection_to_obstacles_dotnet
{
    public struct OccupancyGridCell
    {
        public int value;
        public float x;
        public float y;
    }
}