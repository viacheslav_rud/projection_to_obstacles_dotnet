using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Messages.geometry_msgs;
using Messages.nav_msgs;
using Messages.sensor_msgs;
using Messages.std_msgs;

namespace projection_to_obstacles_dotnet
{
    public interface IOccupancyGridService
    {
        OccupancyGridCells MakeCells(OccupancyGrid occupancyGrid);
        IEnumerable<Vector2> TakeExternal(OccupancyGridCells cells);

        List<List<Vector2>> GroupIslands(IEnumerable<Vector2> vertices, float resolution, float islandSizeMultiplier,
            float islandMergeThresholdMultiplier);

        List<Polygon> MakePolygons(List<List<Vector2>> islands, float resolution);

        PointCloud MakePointCloud(OccupancyGridCells occupancyGridCells);
    }

    public class OccupancyGridService : IOccupancyGridService
    {
        private const int Density = 4;

        private int OccupiedValue { get; } = 100;

        public OccupancyGridCells MakeCells(OccupancyGrid occupancyGrid)
        {
            float resolution = occupancyGrid.info.resolution;
            uint height = occupancyGrid.info.height;
            uint width = occupancyGrid.info.width;

            var cells = new List<OccupancyGridCell>();
            int index = 0;
            for (int i = 0; i < height; ++i)
            for (int j = 0; j < width; ++j)
            {
                var position = occupancyGrid.info.origin.position;
                var cell = new OccupancyGridCell {value = occupancyGrid.data[index], x = (float) (j * resolution + position.x), y = (float) (i * resolution + position.y)};
                cells.Add(cell);
                index++;
            }

            return new OccupancyGridCells {Cells = cells, Height = height, Resolution = resolution, Width = width};
        }

        public IEnumerable<Vector2> TakeExternal(OccupancyGridCells cells)
        {
            for (int i = 0; i < cells.Cells.Count; ++i)
            {
                if (cells.Cells[i].value == OccupiedValue && GetOccupiedNeighboursNumber(cells, i) <= 5)
                {
                    yield return new Vector2(cells.Cells[i].x, (float) cells.Cells[i].y);
                }
            }
        }

        public List<List<Vector2>> GroupIslands(IEnumerable<Vector2> vertices, float resolution, float islandSizeMultiplier, float islandMergeThresholdMultiplier)
        {
            var islands = new List<List<Vector2>>();

            foreach (var vert in vertices)
            {
                bool added = false;
                if (islands.Any())
                    added = TryAddToExistingIsland(islands, vert, resolution, islandSizeMultiplier,
                        islandMergeThresholdMultiplier);

                // make a new island
                if (added) continue;
                var newIsland = new List<Vector2>((int) (islandSizeMultiplier * islandSizeMultiplier)) {vert};
                islands.Add(newIsland);
            }

            return islands;
        }

        public List<Polygon> MakePolygons(List<List<Vector2>> islands, float resolution)
        {
            return islands.Select(GetConvexHull).Select(points => new Polygon {points = points.Select(s => new Point32 {x = s.x, y = s.y}).ToArray()}).ToList();
        }

        public PointCloud MakePointCloud(OccupancyGridCells occupancyGridCells)
        {
            var elapsed = TimeSpan.Zero;
            var cloud = new PointCloud()
            {
                header = new Header {frame_id = "map"},
                points = occupancyGridCells.Cells
                    .Where(s => s.value == OccupiedValue)
                    .Select(s => MakeDensePoints(s, occupancyGridCells.Resolution, ref elapsed))
                    .SelectMany(s => s)
                    .Select(cell => new Point32 {x = cell.x, y = cell.y})
                    .ToArray()
            };
            Console.WriteLine("MakePointCloud: " + elapsed.Milliseconds);
            return cloud;
        }

        private static IEnumerable<Point32> MakeDensePoints(OccupancyGridCell cell, float resolution, ref TimeSpan time)
        {
            var step = resolution / Density;
            var sw = Stopwatch.StartNew();
            var points = new List<Point32>();
            for (float y = 0; y < resolution; y += step)
            {
                for (float x = 0; x < resolution; x += step)
                {
                    points.Add(new Point32 {x = cell.x + x, y = cell.y + y});
                }
            }

            sw.Stop();
            time += sw.Elapsed;
            return points;
        }

        private static double Cross(Vector2 O, Vector2 A, Vector2 B)
        {
            return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
        }

        private static List<Vector2> GetConvexHull(List<Vector2> points)
        {
            if (points.Count <= 1)
                return points;

            int n = points.Count, k = 0;
            var hull = new List<Vector2>(new Vector2[2 * n]);

            points.Sort((a, b) =>
                Math.Abs(a.x - b.x) < 0.1f ? a.y.CompareTo(b.y) : a.x.CompareTo(b.x));

            // Build lower hull
            for (int i = 0; i < n; ++i)
            {
                while (k >= 2 && Cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
                    k--;
                hull[k++] = points[i];
            }

            // Build upper hull
            for (int i = n - 2, t = k + 1; i >= 0; i--)
            {
                while (k >= t && Cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
                    k--;
                hull[k++] = points[i];
            }

            return hull.Take(k - 1).ToList();
        }

        private static double GetDistance(Vector2 v1, Vector2 v2)
        {
            return Math.Sqrt(Math.Abs((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y)));
        }

        private static bool TryAddToExistingIsland(List<List<Vector2>> islands, Vector2 vertex, float resolution,
            float maxSizeMultiplier, float mergeThresholdMultiplier)
        {
            foreach (var island in islands)
            {
                // if any vertex is too far away
                if (island.Any(s => GetDistance(s, vertex) > resolution * maxSizeMultiplier))
                    continue;

                // if any vertex is neighbor
                if (island.Any(s => GetDistance(s, vertex) < resolution * mergeThresholdMultiplier))
                {
                    island.Add(vertex);
                    return true;
                }
            }

            return false;
        }

        private int GetOccupiedNeighboursNumber(OccupancyGridCells ocCells, int cellIndex)
        {
            double threshold = ocCells.Resolution / 2;

            int neighboursNumber = 0;

            var cell = ocCells.Cells[cellIndex];
            for (int i = 0; i < ocCells.Cells.Count; ++i)
            {
                if (cellIndex == i) continue;
                if (Math.Abs(ocCells.Cells[i].x - cell.x) < threshold + ocCells.Resolution
                    && Math.Abs(ocCells.Cells[i].y - cell.y) < threshold + ocCells.Resolution
                    && ocCells.Cells[i].value == OccupiedValue)
                {
                    neighboursNumber++;
                }
            }

            return neighboursNumber;
        }
    }
}