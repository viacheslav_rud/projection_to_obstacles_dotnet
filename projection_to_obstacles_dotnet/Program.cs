#define PERF_LOG
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Messages.nav_msgs;
using Messages.sensor_msgs;
using Uml.Robotics.Ros;

namespace projection_to_obstacles_dotnet
{
    internal static class Program
    {
        private static string url = "0.0.0.0";
        private static bool usePolygons;
        private static bool usePointCloud = true;
        private static float islandSizeMultiplier = 20;
        private static float islandMergeThresholdMultiplier = 2.5f;
        private static string pointCloudTopic = "/navigation/projected_pointcloud";
        private static string obstacleArrayTopic = "/move_base/TebLocalPlannerROS/obstacles";
        private static string markersTopic = "/obstacle_polygons";
        private static string projectedMapTopic = "/projected_map";
        private static string globalFrame = "/map";

        private static OccupancyGridService occupancyGridService;

        private static string pointCloudPublisherId;
        private static string obstacleArrayPublisherId;
        private static string markersPublisherId;

        private static OccupancyGrid last;

        private static Publisher<PointCloud> pointCloudPublisher;

        private static void Main(string[] args)
        {
            Environment.SetEnvironmentVariable("ROS_MASTER_URI", "http://viacheslav-MS-7A34:11311");
            Environment.SetEnvironmentVariable("ROS_HOSTNAME", "localhost");
            ParseArguments(args);
            occupancyGridService = new OccupancyGridService();
            ROS.Init(args, "Talker");
            var spinner = new SingleThreadSpinner();
            var node = new NodeHandle();
            pointCloudPublisher = node.Advertise<PointCloud>(pointCloudTopic, 1);

            node.Subscribe<OccupancyGrid>(projectedMapTopic, 1, Handler);

            while (ROS.OK && !Console.KeyAvailable) spinner.SpinOnce();

            ROS.Shutdown();
        }


        private static void ParseArguments(IReadOnlyCollection<string> args)
        {
            try
            {
                if (args.Count < 1)
                    return;

                var arguments = args.ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
                SetArgument(arguments, "UsePolygons", s => usePolygons = bool.Parse(s));
                SetArgument(arguments, "UsePointCloud", s => usePointCloud = bool.Parse(s));
                SetArgument(arguments, "IslandSizeMultiplier", s => islandSizeMultiplier = float.Parse(s));
                SetArgument(arguments, "IslandMergeThresholdMultiplier", s => islandMergeThresholdMultiplier = float.Parse(s));
                SetArgument(arguments, "PointCloudTopic", s => pointCloudTopic = s);
                SetArgument(arguments, "ProjectedMapTopic", s => projectedMapTopic = s);
                SetArgument(arguments, "GlobalFrame", s => globalFrame = s);
                SetArgument(arguments, "Url", s => url = s);
                SetArgument(arguments, "ObstacleArrayTopic", s => obstacleArrayTopic = s);
                SetArgument(arguments, "MarkersTopic", s => markersTopic = s);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error parsing arguments: {e}");
                throw;
            }
        }

        private static void SetArgument(IReadOnlyDictionary<string, string> arguments, string argumentName,
            Action<string> set)
        {
            if (arguments.ContainsKey(argumentName)) set(arguments[argumentName]);
        }

        private static void Handler(OccupancyGrid msg)
        {
            var sw = Stopwatch.StartNew();
            Process(msg);
            sw.Stop();
            ROS.Info()(sw.ElapsedMilliseconds);
        }

        private static void Process(OccupancyGrid msg)
        {
#if PERF_LOG
            var cw = Stopwatch.StartNew();
#endif

            var cells = occupancyGridService.MakeCells(msg);

            if (usePolygons)
            {
                var external =
                    cells.Cells.Where(s => s.value == 100).Select(s => new Vector2(s.x, s.y))
                        .ToArray();
            }

            if (usePointCloud)
            {
                var pc = occupancyGridService.MakePointCloud(cells);
                pointCloudPublisher.Publish(pc);
            }

#if PERF_LOG
            Console.WriteLine($"Elapsed: {cw.ElapsedMilliseconds} ms");
#endif
        }
    }
}