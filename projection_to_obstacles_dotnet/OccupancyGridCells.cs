using System.Collections.Generic;

namespace projection_to_obstacles_dotnet
{
    public class OccupancyGridCells
    {
        public float Resolution { get; set; }
        public uint Height { get; set; }
        public uint Width { get; set; }

        public List<OccupancyGridCell> Cells { get; set; }
    }
}