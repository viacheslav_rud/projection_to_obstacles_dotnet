projection_to_obstacles_dotnet

It won't be built with catkin_make!!!
To build it as self-contained (don't need to install .NET Core on target machine) run next script:

`projection_to_obstacles_dotnet/Build_Self-Contained.sh`

To build you need to install .NET Core SDK: https://docs.microsoft.com/en-us/dotnet/core/install/linux

Then you can run it with ./projection_to_obstacles_dotnet from Build diectory.
To run it with ros there are run.py and run.launch files. You can either run is with rosrun or with roslaunch

The solution includes also RosBridgeClient and WebSocket Sharp Core packages. They are in NuGet but both of them are not compatible with .NET core. These are clones of official repos, but with little changes.